﻿using System;

namespace TetrisV2
{
	public class Brick
	{
		public int X { get; set; }
		public int Y { get; set; }
	}
}
