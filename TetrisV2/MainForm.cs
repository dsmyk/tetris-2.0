﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace TetrisV2
{
	public partial class MainForm : Form
	{	
		private Game game = new Game();
		
		public MainForm()
		{
			InitializeComponent();
			game.Create_guards();
			game.Show_new_blcok();
		}
		
		void CanvasPaint(object sender, PaintEventArgs e)
		{
			Graphics g = canvas.CreateGraphics();
			game.StartGraphics(g);
		}
		
		void TimerTick(object sender, EventArgs e)
		{
			game.Repetetive_functions();
		}
		
		void MainFormKeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Left)
			{
				game.Move_left();
			}
			if(e.KeyCode == Keys.Right)
			{
				game.Move_right();
			}
			if(e.KeyCode == Keys.Up)
			{
				game.Rotate();
			}
			if(e.KeyCode == Keys.P)
			{
				Timer.Enabled = false;
			}
			if(e.KeyCode == Keys.S)
			{
				Timer.Enabled = true;
			}
		}
		
		void MainFormLoad(object sender, EventArgs e)
		{
			AllocConsole();
		}
		
		//Allows comand line to be seen during normal run
		[DllImport("kernel32.dll",SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool AllocConsole();
	}
}
