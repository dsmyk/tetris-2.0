﻿using System;
using System.Drawing;
using System.Threading;

namespace TetrisV2
{
	public class GEngine
	{
		private Graphics draw_handle;
		private Thread render_thread;
		private Pen black = new Pen(Brushes.Black, 5);
		
		private Game game;
		
		public GEngine(Graphics g, Game gameParam)
		{
			game = gameParam;
			draw_handle = g;
		}
		
		public void Init()
		{
			render_thread = new Thread(new ThreadStart(Render));
			render_thread.Start();
		}

		private void Render()
		{
			
			Bitmap frame = new Bitmap(Game.canvas_width, Game.canvas_height);
			Graphics frame_graphics = Graphics.FromImage(frame);
			
			while(true)
			{
				frame_graphics.FillRectangle(Brushes.Purple, Game.gameboard_top_left_corner_X, Game.gameboard_top_left_corner_Y, Game.gameboard_bottom_right_corner_X - Game.gameboard_top_left_corner_X, Game.gameboard_bottom_right_corner_Y - Game.gameboard_top_left_corner_Y);

				for(int x = 0; x < Game.gameboard_width_in_bricks; x++)
				{
					for(int y = 0; y < Game.gameboard_height_in_bricks; y++)
					{
						if(y >= 4 && game.gameboard[x, y] == 1 || y > 4 && game.gameboard[x, y] == 101)
						{
							frame_graphics.FillRectangle(Brushes.Cyan, Game.gameboard_top_left_corner_X  + (x - 1) * Game.brick_size_in_pixels, Game.gameboard_top_left_corner_Y + (y - 4) * Game.brick_size_in_pixels , Game.brick_size_in_pixels, Game.brick_size_in_pixels);
						}
						if(y >= 4 && game.gameboard[x, y] == 2 || y > 4 && game.gameboard[x, y] == 102)
						{
							frame_graphics.FillRectangle(Brushes.Red, Game.gameboard_top_left_corner_X  + (x - 1) * Game.brick_size_in_pixels, Game.gameboard_top_left_corner_Y + (y - 4) * Game.brick_size_in_pixels , Game.brick_size_in_pixels, Game.brick_size_in_pixels);
						}
						if(y >= 4 && game.gameboard[x, y] == 3 || y > 4 && game.gameboard[x, y] == 103)
						{
							frame_graphics.FillRectangle(Brushes.Gray, Game.gameboard_top_left_corner_X  + (x - 1) * Game.brick_size_in_pixels, Game.gameboard_top_left_corner_Y + (y - 4) * Game.brick_size_in_pixels , Game.brick_size_in_pixels, Game.brick_size_in_pixels);
						}
						if(y >= 4 && game.gameboard[x, y] == 4 || y > 4 && game.gameboard[x, y] == 104)
						{
							frame_graphics.FillRectangle(Brushes.Yellow, Game.gameboard_top_left_corner_X  + (x - 1) * Game.brick_size_in_pixels, Game.gameboard_top_left_corner_Y + (y - 4) * Game.brick_size_in_pixels , Game.brick_size_in_pixels, Game.brick_size_in_pixels);
						}
						if(y >= 4 && game.gameboard[x, y] == 5 || y > 4 && game.gameboard[x, y] == 105)
						{
							frame_graphics.FillRectangle(Brushes.Green, Game.gameboard_top_left_corner_X  + (x - 1) * Game.brick_size_in_pixels, Game.gameboard_top_left_corner_Y + (y - 4) * Game.brick_size_in_pixels , Game.brick_size_in_pixels, Game.brick_size_in_pixels);
						}
						if(y >= 4 && game.gameboard[x, y] == 6 || y > 4 && game.gameboard[x, y] == 106)
						{
							frame_graphics.FillRectangle(Brushes.BurlyWood, Game.gameboard_top_left_corner_X  + (x - 1) * Game.brick_size_in_pixels, Game.gameboard_top_left_corner_Y + (y - 4) * Game.brick_size_in_pixels , Game.brick_size_in_pixels, Game.brick_size_in_pixels);
						}
						if(y >= 4 && game.gameboard[x, y] == 7 || y > 4 && game.gameboard[x, y] == 107)
						{
							frame_graphics.FillRectangle(Brushes.DarkOrange, Game.gameboard_top_left_corner_X  + (x - 1) * Game.brick_size_in_pixels, Game.gameboard_top_left_corner_Y + (y - 4) * Game.brick_size_in_pixels , Game.brick_size_in_pixels, Game.brick_size_in_pixels);
						}
					}
				}
				
				for(int y = 0; y < Game.gameboard_height_in_bricks - 4; y++)
				{
					frame_graphics.DrawLine(black, Game.gameboard_top_left_corner_X, Game.gameboard_top_left_corner_Y + y * Game.brick_size_in_pixels, Game.gameboard_bottom_right_corner_X, 50 + y * Game.brick_size_in_pixels);	
				}
				
				for(int x = 0; x < Game.gameboard_width_in_bricks - 1; x++)
				{
					frame_graphics.DrawLine(black, Game.gameboard_top_left_corner_X + x * Game.brick_size_in_pixels, 50, 300 + x * Game.brick_size_in_pixels, Game.gameboard_bottom_right_corner_Y);
				}
				draw_handle.DrawImage(frame, 0, 0);
			}
		}
	}
}