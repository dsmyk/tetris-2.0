﻿using System;
using System.Collections;

namespace TetrisV2
{
	public class Block
	{
		public ArrayList bricks = new ArrayList();
		
		protected int numeric_value = 0;
		
		protected Brick brick1;
		protected Brick brick2;
		protected Brick brick3;
		protected Brick brick4;
		
		protected int rotate_state = 0;
		
		private int difference_Y;
		private int difference_X;
		
		private Brick basic_brick;
		
		public bool Fall(ref int[,] gameboard, ref int current_brick_Y, ref Boolean board_state_changed)
		{
			board_state_changed = false;
			bool can_fall_down = true;
			
			foreach (Brick brick in bricks)
			{
				if(gameboard[brick.X,brick.Y + 1] != 0 && gameboard[brick.X, brick.Y + 1] != numeric_value)
				{
					can_fall_down = false;
				}
			}
			
			if(can_fall_down)
			{
				for(int y = Game.gameboard_height_in_bricks - 1; y >= 0; y--)
				{
					for(int x = 0; x <= Game.gameboard_width_in_bricks - 1; x++)
					{
						if(gameboard[x, y] == numeric_value)
						{
							gameboard[x, y + 1] = numeric_value;
							gameboard[x, y] = 0;
						}
					}
				}
				
				for(int i = 0; i <= bricks.Count - 1; i++)
				{
					Brick b = (TetrisV2.Brick)bricks[i];
					b.Y += 1;
					gameboard[b.X, b.Y] = numeric_value;
				}
				
				current_brick_Y += 1;
				board_state_changed = true;
			}
			
			else
			{
				for(int i = bricks.Count - 1; i >= 0; i--)
				{
					Brick b = (TetrisV2.Brick)bricks[i];
					gameboard[b.X, b.Y] += 100;
				}
				bricks.Clear();
			}
			return board_state_changed;
		}
		
		public void Move_Left(ref int[,] gameboard, ref int current_brick_X, ref int current_brick_Y, ref Boolean board_state_changed)
		{
   			bool can_move_left = true;

			foreach (Brick brick in bricks)
			{
                if (gameboard[brick.X - 1, brick.Y] != 0 && gameboard[brick.X - 1, brick.Y] != numeric_value)
				{
                    can_move_left = false;
				}
			}

			if (can_move_left)
			{
				for(int y = Game.gameboard_height_in_bricks - 1; y >= 0; y--)
				{
					for(int x = 0; x <= Game.gameboard_width_in_bricks - 1; x++)
					{
						if(gameboard[x, y] == numeric_value)
						{
							gameboard[x - 1, y] = numeric_value;
							gameboard[x, y] = 0;
						}
					}
				}
				
				for(int i = 0; i <= bricks.Count - 1; i++)
				{
					Brick b = (TetrisV2.Brick)bricks[i];
					b.X -= 1;
					gameboard[b.X, b.Y] = numeric_value;
				}
				
				current_brick_X -= 1;
			}
		}
		
		public void Move_Right(ref int[,] gameboard, ref int current_brickX, ref int current_brickY, ref bool board_state_changed)
		{
            bool can_move_right = true;
            
            foreach (Brick brick in bricks)
            {
            	if(gameboard[brick.X + 1, brick.Y] != 0 && gameboard[brick.X + 1, brick.Y] != numeric_value)
            	{
            		can_move_right = false;
            	}
            }
            
            if(can_move_right)
            {
            	for(int y = Game.gameboard_height_in_bricks - 1; y >= 0; y--)
				{
					for(int x = Game.gameboard_width_in_bricks - 1; x >= 0; x--)
					{
						if(gameboard[x, y] == numeric_value)
						{
							gameboard[x + 1, y] = numeric_value;
							gameboard[x, y] = 0;
						}
					}
				}
            	
				for(int i = 0; i <= bricks.Count - 1; i++)
				{
					Brick b = (TetrisV2.Brick)bricks[i];
					b.X += 1;
					gameboard[b.X, b.Y] = numeric_value;
				}
				
				current_brickX += 1;
            }
		}
		
		public void Rotate(ref int[,] gameboard, ref int current_brick_X, ref int current_brick_Y)
		{
			bool brick_was_rotated = false;

			
			basic_brick = new Brick()
			{
				X = current_brick_X,
				Y = current_brick_Y
			};
			
			foreach(Brick brick in bricks)
			{
				difference_Y = basic_brick.Y - brick.Y;
				difference_X = basic_brick.X - brick.X;
				Console.WriteLine();
				Console.Write("Difference X: ");
				Console.WriteLine(difference_X);
				Console.Write("Difference Y: ");
				Console.WriteLine(difference_Y);
				
				brick_was_rotated = false;
				
				if(difference_X + brick.X < Game.gameboard_width_in_bricks - 2 && difference_X + brick.X > 2 && difference_Y + brick.Y < Game.gameboard_height_in_bricks - 3)
				{
					if(numeric_value != 3)
					{
						if(difference_X != 0 && difference_Y != 0)
						{
							if(!brick_was_rotated && difference_X < 0 && difference_Y > 0)
							{
								// prawa gora
								gameboard[brick.X, brick.Y] = 0;
								brick.Y = basic_brick.Y - difference_X;
								gameboard[brick.X, brick.Y] = 1;
								brick_was_rotated = true;
							}
							
							if(!brick_was_rotated && difference_X < 0 && difference_Y < 0)
							{
								// prawy dol
								gameboard[brick.X, brick.Y] = 0;
								brick.X = basic_brick.X + difference_Y;
								gameboard[brick.X, brick.Y] = 1;
								brick_was_rotated = true;
							}
							
							if(!brick_was_rotated && difference_X > 0 && difference_Y < 0)
							{
								// lewy dol
								gameboard[brick.X, brick.Y] = 0;
								brick.Y = basic_brick.Y - difference_X;
								gameboard[brick.X, brick.Y] = 1;
								brick_was_rotated = true;
							}
							
							if(!brick_was_rotated && difference_X > 0 && difference_Y > 0)
							{
								// lewa gora
								gameboard[brick.X, brick.Y] = 0;
								brick.X = basic_brick.X + difference_Y;
								gameboard[brick.X, brick.Y] = 1;
								brick_was_rotated = true;
							}
						}
						
						else if(!brick_was_rotated &&difference_X == 0 || !brick_was_rotated && difference_Y == 0)
						{
							if(!brick_was_rotated && difference_Y > 0 && difference_X == 0)
							{
								gameboard[brick.X, brick.Y] = 0;
								brick.X = basic_brick.X + difference_Y;
								brick.Y = basic_brick.Y;
								gameboard[brick.X, brick.Y] = 1;
								brick_was_rotated = true;
							}
							
							if(!brick_was_rotated && difference_Y < 0 && difference_X == 0)
							{
								gameboard[brick.X, brick.Y] = 0;
								brick.X = basic_brick.X + difference_Y;
								brick.Y = basic_brick.Y;
								gameboard[brick.X, brick.Y] = 1;
								brick_was_rotated = true;
							}
							
							if(!brick_was_rotated && difference_X > 0 && difference_Y == 0)
							{
								gameboard[brick.X, brick.Y] = 0;
								brick.Y = basic_brick.Y - difference_X;
								brick.X = basic_brick.X;
								gameboard[brick.X, brick.Y] = 1;
								brick_was_rotated = true;
		
							}
							
							if(!brick_was_rotated && difference_X < 0 && difference_Y == 0)
							{
								gameboard[brick.X, brick.Y] = 0;
								brick.Y = basic_brick.Y - difference_X;
								brick.X = basic_brick.X;
								gameboard[brick.X, brick.Y] = 1;
								brick_was_rotated = true;
							}
						}
					}
				}
			}
			
			foreach(Brick b in bricks)
			{
				gameboard[b.X, b.Y] = numeric_value;
			}
			
			rotate_state += 1;
			if(rotate_state == 4)
			{
				rotate_state = 0;
			}
		}
		
		private void Test()
		{
			Console.WriteLine("");
			Console.Write("Rotate state: ");
			Console.WriteLine(rotate_state);
			foreach(Brick b in bricks)
			{
				Console.Write("[");
				Console.Write(b.X);
				Console.Write(", ");
				Console.Write(b.Y);
				Console.WriteLine("]");
			}
			Console.WriteLine("");
		}
		
		public void Logs(ref int current_brick_X, ref int current_brick_Y)
		{
			Test();
			Console.WriteLine("Block");
			Console.Write("Numeric value: ");
			Console.WriteLine(numeric_value);
			Console.Write("Number of bricks: ");
			Console.WriteLine(bricks.Count);
			Console.Write("Current brick Y: ");
			Console.WriteLine(current_brick_Y);
			Console.Write("Current brick X: ");
			Console.WriteLine(current_brick_X);
		}
		
	}
}
