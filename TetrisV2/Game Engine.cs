﻿using System;
using System.Drawing;

namespace TetrisV2
{
	public class Game
	{
		
		public int[,] gameboard = new int[gameboard_width_in_bricks, gameboard_height_in_bricks];
		
		public Boolean board_state_changed = false;
		
		public int current_brick_X = 5;
		public int current_brick_Y = 0;
		
		public Block current_block;
		
		public int brick_value;
	
		public static int canvas_height = 700;
		public static int canvas_width = 1000;
		
		public static int gameboard_height_in_bricks = 20;
		public static int gameboard_width_in_bricks = 12;
		public static int brick_size_in_pixels = 40;
		
		public static int gameboard_top_left_corner_X = 300;
		public static int gameboard_top_left_corner_Y = 50;
		
		public static int gameboard_bottom_right_corner_X = 700;
		public static int gameboard_bottom_right_corner_Y = 650;
		
		public static int guards_value = 1000;
		
		
		private GEngine gEngine;
		private int cleared_line_height;
		
		public void Create_guards()
		{
			for (int x = 0; x < gameboard_width_in_bricks; x++)
			{
				for(int y = 0; y < gameboard_height_in_bricks; y++)
				{
					if(x == 0 || x == gameboard_width_in_bricks - 1 || y == gameboard_height_in_bricks - 1)
					{
						gameboard[x, y] = guards_value;
					}
				}
			}
		}
		
		
		public void Repetetive_functions()
		{
			PrintGameBooardToLog();
			current_block.Logs(ref current_brick_X, ref current_brick_Y);
			
			Clear_line_when_full();
			
			if(!board_state_changed)
			{
				current_brick_Y = 0;
				Show_new_blcok();
			}
			
			current_block.Fall(ref gameboard, ref current_brick_Y, ref board_state_changed);
			
		}
		
		private void Clear_line_when_full()
		{
			for(int i = 0; i < gameboard_height_in_bricks - 1; i++)
			{
				if(gameboard[1,i] > 100 && gameboard[2,i] > 100 && gameboard[3,i] > 100 &&
				   gameboard[4,i] > 100 && gameboard[5,i] > 100 && gameboard[6,i] > 100 &&
				   gameboard[7,i] > 100 && gameboard[8,i] > 100 && gameboard[9,i] > 100 &&
				   gameboard[10,i] > 100)
				{
					for(int j = 1; j < gameboard_width_in_bricks - 1; j++)
					{
						gameboard[j,i] = 0;
					}
					cleared_line_height = i;
					
					Make_blocks_fall_by_one_after_line_clear();
				}
			}
		}
		
		public void Make_blocks_fall_by_one_after_line_clear()
        {
        	int block_value;
        	
        	for(int b = cleared_line_height; b >= 0; b --)
        	{
        		for(int a = 1; a < 11; a ++)
        		{
        			if(gameboard[a, b] < guards_value && gameboard[a, b + 1] == 0)
        			{
        				block_value = gameboard[a, b];
        				gameboard[a, b + 1] = block_value;
        				gameboard[a, b] = 0;
        			}
        		}
        	}
        }
		
		public void Show_new_blcok()
		{
			Random rng = new Random();
			int chosen_block = rng.Next(1,8);
			switch(chosen_block)
			{
				case 1:
					{
						current_block = new I_Block(5, 1, ref gameboard, ref current_brick_X, ref current_brick_Y);
						break;
					}
				case 2:
					{
						current_block = new T_Block(5, 0, ref gameboard, ref current_brick_X, ref current_brick_Y);
						break;
					}
				case 3:
					{
						current_block = new O_Block(5, 0, ref gameboard, ref current_brick_X, ref current_brick_Y);
						break;
					}
				case 4:
					{
						current_block = new L_Block(5, 2, ref gameboard, ref current_brick_X, ref current_brick_Y);
						break;
					}
				case 5:
					{
						current_block = new J_Block(5, 2, ref gameboard, ref current_brick_X, ref current_brick_Y);
						break;
					}
				case 6:
					{
						current_block = new S_Block(5, 0, ref gameboard, ref current_brick_X, ref current_brick_Y);
						break;
					}
				case 7:
					{
						current_block = new Z_Block(5, 0, ref gameboard, ref current_brick_X, ref current_brick_Y);
						break;
					}
			}
		}
		
		public void Move_left()
		{
			current_block.Move_Left(ref gameboard, ref current_brick_X, ref current_brick_Y, ref board_state_changed);
		}
		
		public void Move_right()
		{
			current_block.Move_Right(ref gameboard, ref current_brick_X, ref current_brick_Y, ref board_state_changed);
		}
		
		public void Rotate()
		{
			current_block.Rotate(ref gameboard, ref current_brick_X, ref current_brick_Y);
		}
		
		public void StartGraphics(Graphics g)
		{
			gEngine = new GEngine(g, this);
			gEngine.Init();
		}
		
		private void PrintGameBooardToLog()
		{
			for (int b = 0; b < gameboard_height_in_bricks; b++)
			{
				for (int a = 0; a < gameboard_width_in_bricks; a++)
				{
					Console.Write("|");
		
					int val = gameboard[a, b];
					if (val >= 0 && val < 10)
					{
						Console.Write("000");
					}
					else if (val >= 10 && val < guards_value)
					{
						Console.Write("0");
					}
					Console.Write(gameboard[a, b]);
				}
					Console.WriteLine("|");
			}
					Console.WriteLine("-------------------------------------------------------------");
		}
			
	}
}
