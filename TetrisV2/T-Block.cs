﻿using System;
using System.Collections;

namespace TetrisV2
{
	public class T_Block : Block
	{
		public T_Block(int x, int y, ref int[,] gameboard, ref int current_brick_X, ref int current_brick_Y)
		{
			current_brick_X = 5;
			current_brick_Y = 0;
			
			numeric_value =  2;
			rotate_state = 0;
			
			brick1 = new Brick()
			{
				X = x - 1,
				Y = y
			};
			
			brick2 = new Brick()
			{
				X = x,
				Y = y
			};
			
			brick3 = new Brick()
			{
				X = x + 1,
				Y = y 
			};
			
			brick4 = new Brick()
			{
				X = x,
				Y = y + 1
			};
			
			bricks.Add(brick1);
			bricks.Add(brick2);
			bricks.Add(brick3);
			bricks.Add(brick4);
			
			foreach(Brick brick in bricks)
			{
				gameboard[x, y] = numeric_value;
			}
		}
	}
}
